module.exports = {
    devServer: {
        proxy: {
            '/api': {
                target: "http://lk.restretching.net/",
                changeOrigin: true,
                pathRewrite: {
                  '^/api': ''
                }
            }
        }       
    }
}